/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = {
	"Iosevka:size=14"
};
static const char *prompt      = NULL;      /* -p  option; prompt to the left of input field */
static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#bec0e2", "#1b1723" },
	[SchemeSel]  = { "#d8d1b8", "#1b1723" },
	[SchemeOut]  = { "#bec0e2", "#adf5ff" },
	/* [SchemeNorm] = { "#330036", "#adf5ff" }, */
	/* [SchemeSel]  = { "#adf5ff", "#d6396b" }, */
	/* [SchemeOut]  = { "#330036", "#adf5ff" }, */
};
static const unsigned int min_height = 27;
static const double chord_delay = 0.05;
/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines      = 0;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";
